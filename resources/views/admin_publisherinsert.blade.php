@extends('layouts/admin')
@section('content') 


<section class="static about-sec">
        <div class="container">
            <h1>Adding publisher</h1>
            <div class="form">
                <form action="{{url('adminpublisherinsert')}}" method="POST" enctype="multipart/form-data">

{{ csrf_field() }}

                    <div class="form-group">

                      <label for="name">Publisher Name</label>

                        <div class="row">
                            <div class="col-md-5">

                        <input placeholder="Publisher Name" class="form-control input-lg" id="name" name="name" required>
                    </div>
                    </div>
                    </div>


                    
                    <div class="row">    
                        <div class="col-lg-8 col-md-12">
                            <input class="btn btn-primary" type="submit" name="" value="Add publisher">
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </section>

@endsection