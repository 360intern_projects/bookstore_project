
@extends('layouts/public')
@section('content') 
<div class="breadcrumb">
        <div class="container">
            <a class="breadcrumb-item" href="{{url('/')}} ">Home</a>
            <a class="breadcrumb-item" href="{{url('shop')}} ">Shop</a>        
        </div>
</div>

<section class="product-sec">
        <div class="container">
            <h1> {{ $book_get->title }} </h1>
            <div class="row">
                <div class="item " data-slide-number="0">
                    <img src="{{ URL::asset($book_get->image)}}" class="img-fluid">
                </div>
                
                <div class="col-md-6 slider-content">
                    <p>{{ $book_get->description }}</p>
                    <table>
                        
                        <tr>
                            <td>Author Name   </td>
                            <td>{{ $book_get->author->name }}</td>
                        </tr>
                        <tr>
                            <td>Publisher Name</td>
                            <td>{{ $book_get->publisher->name }}</td>
                        </tr>

                    </table>

                    <li><span class="save-cost">Save $7.62 (69%)</span></li>
                        <!-- <li>
                            <span class="name">Publisher Name</span><span class="clm">:</span>
                            <span class="price">{{ $book_get->publisher->name }}</span>
                        </li> -->
                        
                        
                   
                    <div class="btn-sec">
                        <a href="{{ URL::asset($book_get->pdf_link)}}" class="btn" download>Download</a>
                    </div>
                </div>
            </div>
        </div>
</section>


@endsection