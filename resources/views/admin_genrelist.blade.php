@extends('layouts/admin')
@section('content') 

<form>

<div class="container">
    <table class="table table-bordered">
        
         <tr >
            <th style="width: 10px">No</th>
            <th>Genre Name</th>
            <th></th>
        </tr>

        @php $i = 1; @endphp
        @foreach($genre_getall as $genre)
         <tr>
            <td>  {{ $i }}  </td>
            <td>  {{ $genre->type }}  </td>
            <td>
                <a href="{{url('admingenreedit',$genre->id)}}"><input type="button" class="btn btn-primary" name="update" value="Update Publisher">
                </a>
                <a href="{{ url('admingenredelete',$genre->id)}}"><input type="button" class="btn btn-primary" name="delete" value="Delete Publisher">
                </a>
            </td>
        </tr>
        @php $i++; @endphp
        @endforeach
    </table>
    </div>
    </form>
@endsection