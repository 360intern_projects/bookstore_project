@extends('layouts/admin')
@section('content') 

<form>

<div class="container">
    <table class="table table-bordered">
        
         <tr >
            <th style="width: 10px">No</th>
            <th>Author Name</th>
            <th></th>
        </tr>

        @php $i = 1; @endphp
        @foreach($author_getall as $author)
         <tr>
            <td>  {{ $i }}  </td>
            <td>  {{ $author->name }}  </td>


            <td>
                <a href="{{url('adminauthoredit',$author->id)}}"><input type="button" class="btn btn-primary" name="update" value="Update Author">
                </a>
                <a href="{{ url('adminauthordelete',$author->id)}}"><input type="button" class="btn btn-primary" name="delete" value="Delete Author">
                </a>
            </td>
        </tr>
        @php $i++; @endphp
        @endforeach
    </table>
    </div>
    </form>
@endsection