@extends('layouts/admin')
@section('content') 
<div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Insert Book</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
        <form action="{{url('adminbookupdate',$book_get->id)}}" class="form-group" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
                  <div class="box-body">
                    <div class="form-group">

                      <label for="title">Book Title</label>

                        <div class="row">
                            <div class="col-md-5">

                        <input placeholder="Book Title" class="form-control input-lg" id="title" name="title"  value="{{$book_get->title}}" required>
                    </div>
                    </div>
                    </div>
                    
                    <div class="form-group">

                      <label for="desc">Description</label>

                        <div class="row">
                            <div class="col-md-5">

                        <input type="text" placeholder="Description" id="desc" class="form-control" name="description" value="{{$book_get->description}}" required>
                    </div>
                    </div>
                    </div>
                    <div class="form-group">

                      <label for="price">Price</label>

                        <div class="row">
                            <div class="col-md-5">

                        <input type="text" placeholder="Price" class="form-control" id="price" name="price" value="{{$book_get->price}}" required>
                    </div>
                    </div>
                    </div>
                    

                    

                    <div class="form-group">

                        <h6>Select Author name</h6>

                        <div class="row">
                            <div class="col-md-5">

                        <select name="author" class="form-control">
                            @foreach($author_get_all as $author)

                            <option value="{{$author->id}}" {{$book_get->author_id == $author->id ? 'selected="selected"' : '' }}>{{$author->name}}</option>
                            @endforeach

                        </select>
                    </div>
                    </div>
                    </div>
                
                    <div class="form-group">
                        <h6>Select Publisher name</h6>
                        <div class="row">
                            <div class="col-md-5">

                        <select name="publisher" class="form-control">
                            @foreach($publisher_get_all as $publisher)
                            <option value="{{$publisher->id}}" {{$book_get->publisher_id == $publisher->id ? 'selected="selected"' : '' }}>{{$publisher->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    </div>
                    </div>

                    <div class="form-group">
                        <h6>Select Genre</h6>
                        <div class="row">
                            <div class="col-md-5">

                        <select name="genre" class="form-control">
                            @foreach($genre_all as $genre)

                            <option value="{{$genre->id}}">{{$genre->type}}</option>
                            <option value="{{$genre->id}}" {{$book_get->genre_id == $genre->id ? 'selected="selected"' : '' }}>{{$genre->type}}</option>
                            @endforeach
                        </select>
                    </div>
                    </div>
                    </div>



                    <div class="form-group">
                      <label for="img">Book Image</label>
                        <div class="row">
                            <div class="col-md-5">

                             <input type="file" class="form-control" id="img" name="image">

                    </div>
                    </div>
                    </div>
                    <div class="form-group">
                      <label for="file">PDF File</label>
                        <div class="row">
                            <div class="col-md-5">

                      <input type="file" class="form-control" id="file" name="pdf">

                    </div>
                    </div>
                    </div>
                    
                    
                  </div><!-- /.box-body -->
                    <div class="row">
                  <div class="box-footer">
                    <div class="col-md-5">

                    <input type="submit" class=" col-md-5 btn btn-primary" value="Update">
                  </div>
              </div>
          </div>
        </form>
</div>

@endsection