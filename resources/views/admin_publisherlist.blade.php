
@extends('layouts/admin')
@section('content') 

<form>

<div class="container">
    <table class="table table-bordered">
        
         <tr >
            <th style="width: 10px">No</th>
            <th>Publisher Name</th>
            <th></th>
        </tr>

        @php $i = 1; @endphp
        @foreach($publisher_all as $publisher)
         <tr>
            <td>  {{ $i }}  </td>
            <td>  {{ $publisher->name }}  </td>
            <td>
                <a href="{{url('adminpublisheredit',$publisher->id)}}"><input type="button" class="btn btn-primary" name="update" value="Update Publisher">
                </a>
                <a href="{{ url('adminpublisherdelete',$publisher->id)}}"><input type="button" class="btn btn-primary" name="delete" value="Delete Publisher">
                </a>
            </td>
        </tr>
        @php $i++; @endphp
        @endforeach
    </table>
    </div>
    </form>
@endsection