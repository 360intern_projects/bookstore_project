@extends('layouts/admin')
@section('content') 
<div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title">Insert Book</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
        <form action="{{url('adminbookinsert')}}" class="form-group" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
                  <div class="box-body">
                    <div class="form-group">

                      <label for="title">Book Title</label>

                        <div class="row">
                            <div class="col-md-5">

                        <input placeholder="Book Title" class="form-control input-lg" id="title" name="title" required>
                    </div>
                    </div>
                    </div>
                    
                    <div class="form-group">

                      <label for="desc">Description</label>

                        <div class="row">
                            <div class="col-md-5">

                        <input type="text" placeholder="Description" id="desc" class="form-control" name="description" required>
                    </div>
                    </div>
                    </div>
                    <div class="form-group">

                      <label for="price">Price</label>

                        <div class="row">
                            <div class="col-md-5">

                        <input type="text" placeholder="Price" class="form-control" id="price" name="price" required>
                    </div>
                    </div>
                    </div>
                    

                    

                    <div class="form-group">

                        <h6>Select Author name</h6>

                        <div class="row">
                            <div class="col-md-5">

                        <select name="author" class="form-control">
                            @foreach($author_get_all as $author)

                            <option value="{{$author->id}}">{{$author->name}}</option>
                            
                            @endforeach

                        </select>
                    </div>
                    </div>
                    </div>
                
                    <div class="form-group">
                        <h6>Select Publisher name</h6>
                        <div class="row">
                            <div class="col-md-5">

                        <select name="publisher" class="form-control">
                            @foreach($publisher_get_all as $publisher)

                            <option value="{{$publisher->id}}">{{$publisher->name}}</option>
                            
                            @endforeach
                        </select>
                    </div>
                    </div>
                    </div>

                    <div class="form-group">
                        <h6>Select Genre</h6>
                        <div class="row">
                            <div class="col-md-5">

                        <select name="genre" class="form-control">
                            @foreach($genre_all as $genre)

                            <option value="{{$genre->id}}">{{$genre->type}}</option>
                            
                            @endforeach
                        </select>
                    </div>
                    </div>
                    </div>



                    <div class="form-group">
                      <label for="img">Book Image</label>
                        <div class="row">
                            <div class="col-md-5">

                             <input type="file" class="form-control" id="img" name="image">

                    </div>
                    </div>
                    </div>
                    <div class="form-group">
                      <label for="file">PDF File</label>
                        <div class="row">
                            <div class="col-md-5">

                      <input type="file" class="form-control" id="file" name="pdf">

                    </div>
                    </div>
                    </div>
                    
                    
                  </div><!-- /.box-body -->
                    <div class="row">
                  <div class="box-footer">
                    <div class="col-md-5">

                    <input type="submit" class=" col-md-5 btn btn-primary" value="Insert">
                  </div>
              </div>
          </div>
        </form>
</div>

@endsection