@extends('layouts.admin')

@section('content')
<head>
  

</head>
<div class="box">
                <div class="box-header">
                  <h3 class="box-title">Data Table With Full Features</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="dtable" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Title</th>
                        <th>Price</th>
                        <th>Description</th>
                        <th>Author</th>
                        <th>Publisher</th>
                      </tr>
                    </thead>
                    <tbody>
                        @foreach($book_getall as $book)

                        <tr>
                        <td>{{$book->title}}</td>
                        <td>{{$book->price}}</td>
                        <td>{{$book->description}}</td>
                        <td> {{$book->author->name}}</td>
                        <td>{{$book->publisher->name}}</td>
                      </tr>
                @endforeach
                      
                      
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>Rendering engine</th>
                        <th>Browser</th>
                        <th>Platform(s)</th>
                        <th>Engine version</th>
                        <th>CSS grade</th>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
@endsection
