@extends('layouts/main')

@section('content')
<section class="recomended-sec">
        <div class="container">
            <div class="title">
                <h2>Highly recommendes books</h2>
                <hr>
            </div>
            <div class="row">
                @foreach($book_getall as $book)



                <div class="col-lg-3 col-md-6">
                    <div class="item">
                        <img src="{{$book->image}}" alt="img">
                        <h3>how to be a bwase</h3>
                        <h6><span class="price">{{$book->price}}</span> / <a href="{{ URL::asset($book->pdf_link)}}">Download</a></h6>
                        <div class="hover">
                            <a href="{{ url('detail', $book->id)}}">
                            <span><i class="fa fa-long-arrow-right" aria-hidden="true"></i></span>
                            </a>
                        </div>
                    </div>
                </div>
                @endforeach

            </div>
        </div>
    </section>



			<!--  -->
 	<section class="recent-book-sec">
        <div class="container">
            <div class="title">
                <h2>Recents books</h2>
                <hr>
            </div>
            <div class="row">
                @foreach($book_getall as $book)

                <div class="col-lg-2 col-md-3 col-sm-4">
                    <div class="item">
                        <img src="{{$book->image}}" alt="img">
                        <h3><a href="{{ url('detail', $book->id)}}">{{$book->title}}</a></h3>
                        <h6><span class="price">{{$book->price}}</span> / <a href="{{ URL::asset($book->pdf_link)}}" download>Download</a></h6>
                    </div>
                </div>
                @endforeach
                
            </div>
            <div class="btn-sec">
                <a href="{{url('shop')}}" class="btn gray-btn">view all books</a>
            </div>
        </div>
    </section>
@endsection
