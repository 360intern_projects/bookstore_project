@extends('layouts/public')

@section('content')

			<!--  -->
 	<section class="recent-book-sec">
        <div class="container">
            <div class="title">
                <h2>highly recommendes books</h2>
                <hr>
            </div>
            <div class="row">
                @foreach($book_getall as $book)

                <div class="col-lg-2 col-md-3 col-sm-4">
                    <div class="item">
                        <img src="{{ URL::asset($book->image)}}" alt="img" width="200px" height="250px">
                        <h3><a href="{{ url('detail', $book->id)}}">{{$book->title}}</a></h3>
                        <h6><span class="price">{{$book->price}}</span> / <a href="{{ URL::asset($book->pdf_link)}}" download>Download</a></h6>
                    </div>
                </div>
                @endforeach
                
            </div>
            
            <div class="footer">
                <hr>
            </div>
        </div>
    </section>
@endsection
