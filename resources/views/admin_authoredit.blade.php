@extends('layouts/admin')
@section('content') 


<section class="static about-sec">
        <div class="container">
            <h1>Edit Author</h1>
            <div class="form">
                <form action="{{url('adminauthorupdate',$author_get->id)}}" method="POST" enctype="multipart/form-data">

{{ csrf_field() }}

                    <div class="form-group">

                      <label for="name">Author Name</label>

                        <div class="row">
                            <div class="col-md-5">

                        <input placeholder="Author Name" class="form-control input-lg" id="name" name="name" required value="{{$author_get->name}}">
                    </div>
                    </div>
                    </div>


                    
                    <div class="row">    
                        <div class="col-lg-8 col-md-12">
                            <input class="btn btn-primary" type="submit" name="" value="Update Author">
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </section>

@endsection