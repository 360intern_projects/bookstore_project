@extends('layouts.admin')

@section('content')

<section class="recent-book-sec">
        <div class="container">
            <div class="title">
                <h2>highly recommendes books</h2>
                <hr>
            </div>
            <div class="row">
                @foreach($book_getall as $book)

                <div class="col-md-3 col-sm-4">
                <div class="box">
                    <div class="box-header with-border">
                        <img src="{{$book->image}}" alt="img" width="240px" height="300px"></div>
                    <div class="box-body">
                         <h4><a href="{{ url('detail', $book->id)}}">{{$book->title}}</a></h4>
                        <h6><span class="price">{{$book->price}}</span></h6>

                    </div>
                    <div class="box-footer">
                        <a href="{{ url('adminbookedit',$book->id)}}"><input type="submit" class="btn btn-primary" name="update" value="Update">
                        </a> 
                        <a href="{{ url('adminbookdelete',$book->id)}}">
                        <input type="submit" class="btn btn-danger" name="delete" value="Delete">
                        </a>

                    </div>
                </div>
            </div>
                <!-- <div class="col-lg-2 col-md-3 col-sm-4">
                    <div class="">
                        <img src="{{$book->image}}" alt="img" width="200px" height="250px">
                        <h3><a href="{{ url('detail', $book->id)}}">{{$book->title}}</a></h3>
                        <h6><span class="price">{{$book->price}}</span></h6>

                        <a href="{{ url('adminbookedit',$book->id)}}"><input type="submit" class="btn btn-primary" name="update" value="Update">
                        </a> 
                        <a href="{{ url('adminbookdelete',$book->id)}}">
                        <input type="submit" class="btn btn-danger" name="delete" value="Delete">
                        </a>
                    </div>
                </div> -->
                @endforeach
                
            </div>
        </div>
    </section>
@endsection
