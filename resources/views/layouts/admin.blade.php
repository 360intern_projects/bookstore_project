<!DOCTYPE html>
<html>

<title>BookStore</title>
    <head>
        @include('layouts.admin.headcss')
        @include('layouts.admin.script')
    </head>

    <body class="skin-yellow sidebar-mini">
        <div class="wrapper">
            @include('layouts.admin.nav')
            @include('layouts.admin.sidenav')
            <div class="content-wrapper">
            @yield('content')
            </div>
        </div><!-- ./wrapper -->    
    </body>

    @include('layouts.admin.footer')

    <!-- FOR JS -->
    @yield('script')

</html>
