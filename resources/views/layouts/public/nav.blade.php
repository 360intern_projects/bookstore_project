<header>
        <div class="header-top">
            <div class="container">
                <div class="row">
                    <div class="col-md-3"><a href="#" class="web-url">www.bookstore.com</a></div>
                    <div class="col-md-6">
                        <h5>Free Shipping Over $99 + 3 Free Samples With Every Order</h5></div>
                    <div class="col-md-3">
                        <span class="ph-number">Call : 800 1234 5678</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-menu">
            <div class="container">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <a class="navbar-brand" href="{{ url('/')}}"><img src="{{URL::asset('images/logo.png')}}" alt="logo"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ml-auto">
                            <li class="navbar-item active">
                                <a href="{{ url('/')}}" class="nav-link">Home</a>
                            </li>
                            <li class="navbar-item">
                                <a href="{{ url('/shop')}}" class="nav-link">Shop</a>
                            </li>
                            <li class="navbar-item">
                                <a href="{{ url('/')}}" class="nav-link">About</a>
                            </li>
                            
                            @if(Auth::id())
                            <li class="navbar-item">
                               <form method="POST" action="{{ route('logout') }}">
                                  @csrf
                                  <button style="background:none;border:none" type="submit">Logout</button>
                                </form>
                            </li>
                            @else
                            <li class="navbar-item">
                                <a href="{{ route('login') }}" class="nav-link">Login</a>
                            </li>
                            @endif
                        </ul>
                        <div class="cart my-2 my-lg-0">
                            <span>
                                <i class="fa fa-shopping-cart" aria-hidden="true"></i></span>
                            <span class="quntity">3</span>
                        </div>
                        <form class="form-inline my-2 my-lg-0">
                            <input class="form-control mr-sm-2" type="search" placeholder="Search here..." aria-label="Search">
                            <span class="fa fa-search"></span>
                        </form>
                    </div>
                </nav>
            </div>
        </div>
    </header>