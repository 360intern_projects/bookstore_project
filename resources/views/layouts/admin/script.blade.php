<script src="{{ URL::asset('admin_js/jquery.min.js')}}"></script>
<script src="{{ URL::asset('admin_js/bootstrap.min.js')}}"></script>
<script src="{{ URL::asset('admin_js/dashboard.js')}}"></script>
<script src="{{ URL::asset('admin_js/app.js')}}"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <!-- DATA TABES SCRIPT -->
    <script src="{{ URL::asset('plugins/datatables/jquery.dataTables.min.js')}}" type="text/javascript"></script>
    <script src="{{ URL::asset('plugins/datatables/dataTables.bootstrap.min.js')}}" type="text/javascript"></script>
    <!-- SlimScroll -->
    <script src="{{ URL::asset('plugins/slimScroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
    <!-- FastClick -->
    <script src="{{ URL::asset('plugins/fastclick/fastclick.min.js')}}"></script>
    <!-- AdminLTE App -->
   
    <script type="text/javascript">
      $(function () {
        $("#dtable").dataTable();
        $('#dt').dataTable({
          "bPaginate": true,
          "bLengthChange": false,
          "bFilter": false,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false
        });
      });
    </script>