      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="{{URL::asset('images/profile.png')}}" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
              <p>{{Auth::user()->name}}</p>

              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- search form -->
          <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form>
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li>
              <a href="#">
                <i class="fa fa-dashboard"></i> <span>Book</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="active"><a href="{{ url('/home')}}"><i class="fa fa-circle-o"></i> Book List</a></li>
                <li><a href="{{ url('/adminbookcreate')}}"><i class="fa fa-circle-o"></i> Insert Book</a></li>
                
                <li><a href="{{ url('/adminbooktable')}}"><i class="fa fa-circle-o"></i> Book Table</a></li>

              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-dashboard"></i> <span>Author</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{ url('adminauthorlist')}}"><i class="fa fa-circle-o"></i> Author List</a></li>
                <li><a href="{{ url('/adminauthorcreate')}}"><i class="fa fa-circle-o"></i> Insert Author</a></li>
              
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-dashboard"></i> <span>Publisher</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{ url('/adminpublisherlist')}}"><i class="fa fa-circle-o"></i> Publisher List</a></li>
                <li><a href="{{ url('/adminpublishercreate')}}"><i class="fa fa-circle-o"></i> Insert Publisher</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-dashboard"></i> <span>Genre</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{ url('admingenrelist')}}"><i class="fa fa-circle-o"></i> Genre List</a></li>
                <li><a href="{{ url('/shop')}}"><i class="fa fa-circle-o"></i> Insert Genre</a></li>
            </ul>
            </li>           
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>