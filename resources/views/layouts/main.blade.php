<!DOCTYPE html>
<html>

<title>BookStore</title>
    <head>
        @include('layouts.public.headcss')
        @include('layouts.public.script')
    </head>

    <body>
            @include('layouts.public.nav')
            @include('layouts.public.slider')


            @yield('content')
    </body>

    @include('layouts.public.footer')

    <!-- FOR JS -->
    @yield('script')

</html>