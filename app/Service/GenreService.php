<?php 
namespace App\Service;
use App\Model\GenreModel;

	class GenreService 
	{
	    public function get_all()
		{
			return GenreModel::get();
		}

		public function get($id)
		{
			return GenreModel::find($id);
		}

		public function update($arr,$id)
		{
    		GenreModel::where('id',$id)->update($arr);
		}

		public function delete($id)
		{
    		GenreModel::where('id',$id)->delete();
		}
		public function insert($request)
		{
			$genre=new GenreModel();
			$genre->name=$request->name;
			$genre->save();
		}


	}
?>
