<?php 
namespace App\Service;
use App\Model\PublisherModel;

	class PublisherService 
	{
	    public function get_all()
		{
			return PublisherModel::get();
		}
		

		public function get($id)
		{
			return PublisherModel::find($id);
		}

		public function update($arr,$id)
		{
    		PublisherModel::where('id',$id)->update($arr);
		}


public function delete($id)
		{
    		PublisherModel::where('id',$id)->delete();
		}
		public function insert($request)
		{
			$publisher=new PublisherModel();
			$publisher->name=$request->name;
			$publisher->save();
		}
	}
?>
