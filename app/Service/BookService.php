<?php 
namespace App\Service;
use App\Model\BookModel;

	class BookService 
	{
	    public function get_all()
		{
			return BookModel::with('author','publisher')->get();
		}
		

		public function get($id)
		{
			return BookModel::with('author','publisher')->find($id);
		}
		public function insert($book)
		{
			
			
			$book->save();
		}

		public function update($arr,$id)
		{
    		BookModel::where('id',$id)->update($arr);
		}
		public function delete($id)
		{
    		BookModel::where('id',$id)->delete();
		}
		public function apiInsert($book){
			$book->save();
		}

	}
?>
