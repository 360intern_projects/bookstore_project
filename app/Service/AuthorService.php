<?php 
namespace App\Service;
use App\Model\AuthorModel;

	class AuthorService 
	{
	    public function get_all()
		{
			return AuthorModel::get();
		}
		

		public function get($id)
		{
			return AuthorModel::find($id);
		}

		public function update($arr,$id)
		{
    		AuthorModel::where('id',$id)->update($arr);
		}

		public function delete($id)
		{
    		AuthorModel::where('id',$id)->delete();
		}
		public function insert($request)
		{
			$author=new AuthorModel();
			$author->name=$request->name;
			$author->save();
		}


	}
?>
