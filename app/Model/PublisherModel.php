<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PublisherModel extends Model
{
    //

    protected $table = 'publisher';
public function book()
{
return $this->hasMany('App\Model\BookModel','publisher_id');
}

}
