<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BookModel extends Model
{
    //
    protected $table = 'book';
    use SoftDeletes;
    protected $dates =['deleted_at'];

public function author()
{
return $this->belongsTo('App\Model\AuthorModel','author_id');
}
public function publisher()
{
return $this->belongsTo('App\Model\PublisherModel','publisher_id');
}
public function genre()
{
return $this->belongsTo('App\Model\GenreModel','genre_id');
}


}
