<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service\AuthorService;

class AuthorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->AuthorService = new AuthorService();

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $data['author_getall'] = $this->AuthorService->get_all();
        return view('admin_authorlist')->with($data);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


        return view('admin_authorinsert');

        //admin_authorinsert.blade
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //


        $this->AuthorService->insert($request);
         return redirect('/adminauthorlist');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $data['author_get']=$this->AuthorService->get($id);
        return view('admin_authoredit')->with($data);

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $arr['name'] = $request->name;
        $this->AuthorService->update($arr,$id);
        return redirect('/adminauthorlist');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $this->AuthorService->delete($id);
        return redirect('adminauthorlist');
        //
    }
}
