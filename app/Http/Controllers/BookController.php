<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service\GenreService;
use App\Service\BookService;
use App\Service\PublisherService;
use App\Service\AuthorService;
use Illuminate\Support\Facades\Validator;
use App\Model\BookModel;


class BookController extends Controller
{

    public function __construct()
    {
       
        $this->middleware('auth');
        $this->GenreService = new GenreService();
        $this->BookService = new BookService();
        $this->AuthorService = new AuthorService();
        $this->PublisherService = new PublisherService();

    }

  
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $data['book_getall'] = $this->BookService->get_all();
        //dd($book_getall);

        return view('admin_booktable')->with($data);

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['publisher_get_all'] = $this->PublisherService->get_all();
        $data['author_get_all']=$this->AuthorService->get_all();
        $data['book_get_all'] = $this->BookService->get_all();
        $data['genre_all']= $this->GenreService->get_all();
        return view('admin_bookinsert')->with($data);
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,[
            'title'=>'required|max:255',
            'description'=>'required|max:1024',
            'price'=>'required|max:255',
            'author'=>'required|max:255',
            'publisher'=>'required|max:255',
            'genre'=>'required|max:255'
        ]);



        $book=new BookModel();
            $book->title = $request->title;
            $book->author_id = $request->author;
            $book->publisher_id = $request->publisher;
            $book->description  = $request->description;
            $book->price  = $request->price;
            $book->genre_id  = $request->genre;

//        dd('test');
        $this->BookService->insert($book);
         return redirect('/home');

    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data['genre_all']= $this->GenreService->get_all();
        $data['publisher_get_all'] = $this->PublisherService->get_all();
        $data['author_get_all']=$this->AuthorService->get_all();
        $data['book_get_all'] = $this->BookService->get_all();


        $data['book_get'] = $this->BookService->get($id);
        //dd($data['book_get']);

        return view('admin_bookedit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        
        $arr['title'] = $request->title;
        $arr['description'] = $request->description;
        $arr['price'] = $request->price;

        $arr['author_id']=$request->author;
        $arr['publisher_id']=$request->publisher;
        $data['book_get'] = $this->BookService->update($arr,$id);
        return redirect('home');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $this->BookService->delete($id);
        return redirect('home');


    }


}
