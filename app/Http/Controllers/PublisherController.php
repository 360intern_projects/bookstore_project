<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service\PublisherService;


class PublisherController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->PublisherService = new PublisherService();

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

                $data['publisher_all'] = $this->PublisherService->get_all();
        return view('admin_publisherlist')->with($data);

        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin_publisherinsert');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->PublisherService->insert($request);
         return redirect('/adminpublisherlist');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data['publisher_get']=$this->PublisherService->get($id);

        return view('admin_publisheredit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $arr['name'] = $request->name;
        $this->PublisherService->update($arr,$id);
        return redirect('adminpublisherlist');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->PublisherService->delete($id);
        return redirect('adminpublisherlist');
        //
    }
}
