<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service\BookService;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
                $this->BookService = new BookService();

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['book_getall'] = $this->BookService->get_all();
        return view('home')->with($data);
    }
}
