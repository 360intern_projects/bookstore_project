<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Service\BookService;
use App\Service\PublisherService;
use App\Service\AuthorService;
use Illuminate\Support\Facades\DB;

class PublicBookController extends Controller
{

     function __construct()
    {
        $this->BookService = new BookService();
        $this->AuthorService = new AuthorService();
        $this->PublisherService = new PublisherService();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        

        $data['book_getall'] = DB::table('book')->paginate(4);
        return view('public_mainpage')->with($data);
    }



    public function showAll()
    {
        

        $data['book_getall'] = $this->BookService->get_all();
        return view('public_shop')->with($data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $data['book_get'] = $this->BookService->get($id);
        //dd($data['book_get']);

        return view('public_bookdetail')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
