<?php
namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class ApiMail extends Controller
{


public function sendmail(Request $request){
$name = $request->name;
$email = $request->email;
$code=$request->code;
$data = array('name'=>$name, "code" => $code);
    
Mail::send('emails.welcome', $data, function($message) use ($name, $email) {
    $message->to($email, $name)
            ->subject('Book Store Verification');
    $message->from('cosmas259@gmal.com','Mr.Cosmas');
});
 return response()->json(['success'=>"success"]);   

}
}