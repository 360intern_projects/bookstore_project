<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\User; 
use Illuminate\Support\Facades\Hash;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator; 
class UserController extends Controller 
{
public $successStatus = 200;
/** 
     * login api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function login(){

        $user=User::where('email',request('email'))->first();

        if ($user) {
            # code...

            if (Hash::check(request('password'),$user->password)) {
                $success['token'] =  $user->createToken('Bookstore')->accessToken;
                $success['name']=$user->name;
                $success['id']=$user->id; 
                return response()->json(['success' => $success], $this->successStatus); 

            }
            else{

                return response()->json(['error'=>'Password missmatch'],401);

            }
        }
        else{
            return response()->json(['error'=>'User does not exist'],401);
        }

    }
/** 
     * Register api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function register(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'email' => 'required|email', 
            'password' => 'required|min:6', 
            'confirm_password' => 'required|same:password', 
        ]);

        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }

        $input = $request->all(); 
        $input['password'] = Hash::make($input['password']); 
        $user = User::create($input); 
        
        $success['token'] = $user->createToken('Bookstore')-> accessToken; 
        $success['name']=$user->name;
        $success['id']=$user->id;
        return response()->json(['success'=>$success], $this-> successStatus); 
    }


    public function valid(Request $request)
    {

        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'email' => 'required|email', 
            'password' => 'required|min:6', 
            'confirm_password' => 'required|same:password', 
        ]);

        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }
        else{

        return response()->json(['success'=>"success"]);   
        }

    }
/** 
     * details api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function details() 
    { 
        $user = Auth::user(); 
        return response()->json(['success' => $user], $this-> successStatus); 
    } 
}