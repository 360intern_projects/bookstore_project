<?php

use Illuminate\Database\Seeder;

class BookTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('book')->insert([
            [
                'author_id' => 1,
                'publisher_id' => 1,
                'genre_id' =>1,
                'title' => 'How to be a BAWSE',
                'price' => 1000,
                'image' => 'public/images/img1.jpg',
                'pdf_link'=>'public/pdf/laravel.pdf',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys printer took a galley of type and Scrambled it to make a type and typesetting industry. Lorem Ipsum has been the book.'
            ],
            [
                'author_id' => 2,
                'publisher_id' => 2,
                'genre_id' =>2,
                'title' => 'How to be a BAWSE',
                'price' => 1000,
                'image' => 'public/images/img2.jpg',
                'pdf_link'=>'public/pdf/laravel.pdf',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys printer took a galley of type and Scrambled it to make a type and typesetting industry. Lorem Ipsum has been the book.'
            ],
            [
                'author_id' => 3,
                'publisher_id' => 3,
                'genre_id' =>3,
                'title' => 'How to be a BAWSE',
                'price' => 1000,
                'image' => 'public/images/img3.jpg',
                'pdf_link'=>'public/pdf/laravel.pdf',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys printer took a galley of type and Scrambled it to make a type and typesetting industry. Lorem Ipsum has been the book.'
            ],
            [
                'author_id' => 4,
                'publisher_id' => 4,
                'genre_id' =>4,
                'title' => 'How to be a BAWSE',
                'price' => 1000,
                'image' => 'public/images/img4.jpg',
                'pdf_link'=>'public/pdf/laravel.pdf',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys printer took a galley of type and Scrambled it to make a type and typesetting industry. Lorem Ipsum has been the book.'
            ],
            [
                'author_id' => 5,
                'publisher_id' => 5,
                'genre_id' =>5,
                'title' => 'How to be a BAWSE',
                'price' => 1000,
                'image' => 'public/images/r1.jpg',
                'pdf_link'=>'public/pdf/laravel.pdf',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys printer took a galley of type and Scrambled it to make a type and typesetting industry. Lorem Ipsum has been the book.'
            ],
            [
                'author_id' => 6,
                'publisher_id' => 6,
                'genre_id' =>6,
                'title' => 'How to be a BAWSE',
                'price' => 1000,
                'image' => 'public/images/r2.jpg',
                'pdf_link'=>'public/pdf/laravel.pdf',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys printer took a galley of type and Scrambled it to make a type and typesetting industry. Lorem Ipsum has been the book.'
            ],
            [
                'author_id' => 7,
                'publisher_id' => 7,
                'genre_id' =>7,
                'title' => 'How to be a BAWSE',
                'price' => 1000,
                'image' => 'public/images/r3.jpg',
                'pdf_link'=>'public/pdf/laravel.pdf',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys printer took a galley of type and Scrambled it to make a type and typesetting industry. Lorem Ipsum has been the book.'
            ],
            [
                'author_id' => 8,
                'publisher_id' => 8,
                'genre_id' =>8,
                'title' => 'How to be a BAWSE',
                'price' => 1000,
                'image' => 'public/images/r4.jpg',
                'pdf_link'=>'public/pdf/laravel.pdf',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys printer took a galley of type and Scrambled it to make a type and typesetting industry. Lorem Ipsum has been the book.'
            ],
            [
                'author_id' => 9,
                'publisher_id' => 9,
                'genre_id' =>9,
                'title' => 'How to be a BAWSE',
                'price' => 1000,
                'image' => 'public/images/r5.jpg',
                'pdf_link'=>'public/pdf/laravel.pdf',
                'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys printer took a galley of type and Scrambled it to make a type and typesetting industry. Lorem Ipsum has been the book.'
            ],
        ]);

    }
}
