<?php

use Illuminate\Database\Seeder;

class AuthorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('author')->insert([
            [
                'name' => 'Lilly Singh',
            ],
            [
                'name' => 'Joshua',
            ],
            [
                'name' => 'Marty',
            ],
            [
                'name' => 'Wendy',
            ],
            [
                'name' => 'Ashwin Sanghi',
            ],
            [
                'name' => 'Kavita Kane',
            ],
            [
                'name' => 'Barry Eisler',
            ],
            [
                'name' => 'Marc Levy',
            ],
            [
                'name' => 'Ankita Verma',
            ]
        ]);

    }
}
