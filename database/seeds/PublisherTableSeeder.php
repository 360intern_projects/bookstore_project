<?php

use Illuminate\Database\Seeder;

class PublisherTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        DB::table('publisher')->insert([
            [
                'name' => 'publisher 1',
            ],
            [
                'name' => 'publisher 2',
            ],
            [
                'name' => 'publisher 3',
            ],
            [
                'name' => 'publisher 4',
            ],
            [
                'name' => 'publisher 5',
            ],
            [
                'name' => 'publisher 6',
            ],
            [
                'name' => 'publisher 7',
            ],
            [
                'name' => 'publisher 8',
            ],
            [
                'name' => 'publisher 9',
            ]
        ]);

    }
}
