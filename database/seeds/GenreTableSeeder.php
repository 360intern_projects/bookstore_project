<?php

use Illuminate\Database\Seeder;

class GenreTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        DB::table('genre')->insert([
            [
                'type' => 'Genre 1',
            ],
            [
                'type' => 'Genre 2',
            ],
            [
                'type' => 'Genre 3',
            ],
            [
                'type' => 'Genre 4',
            ],
            [
                'type' => 'Genre 5',
            ],
            [
                'type' => 'Genre 6',
            ],
            [
                'type' => 'Genre 7',
            ],
            [
                'type' => 'Genre 8',
            ],
            [
                'type' => 'Genre 9',
            ]
        ]);

    }
}
