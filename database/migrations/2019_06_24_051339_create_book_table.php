<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('book', function (Blueprint $table) {
           $table->increments('id');
            $table->string('title', 50);
            $table->integer('price');
            $table->text('image')->nullable();
            $table->text('pdf_link')->nullable();
            $table->text('description')->nullable();
            $table->integer('author_id');
            $table->integer('publisher_id');
            $table->integer('genre_id');
            $table->softDeletes();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book');
    }
}
