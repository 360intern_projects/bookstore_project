<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('public_mainpage');
});*/


//startpage
Route::get('/','PublicBookController@index');
Auth::routes();


//public
Route::get('detail/{id}','PublicBookController@show');
Route::get('shop','PublicBookController@showAll');



//admin
Route::get('/home', 'HomeController@index')->name('home');

Route::get('adminbookcreate','BookController@create');
Route::post('adminbookinsert','BookController@store');
Route::get('adminbookedit/{id}','BookController@edit');
Route::post('adminbookupdate/{id}','BookController@update');
Route::get('adminbookdelete/{id}','BookController@destroy');
Route::get('adminbooktable','BookController@index');



Route::get('adminauthorlist','AuthorController@index');
Route::get('adminauthorcreate','AuthorController@create');
Route::post('adminauthorinsert','AuthorController@store');
Route::get('adminauthoredit/{id}','AuthorController@edit');
Route::post('adminauthorupdate/{id}','AuthorController@update');
Route::get('adminauthordelete/{id}','AuthorController@destroy');








Route::get('adminpublisherlist','PublisherController@index');
Route::get('adminpublishercreate','PublisherController@create');
Route::post('adminpublisherinsert','PublisherController@store');
Route::get('adminpublisheredit/{id}','PublisherController@edit');
Route::post('adminpublisherupdate/{id}','PublisherController@update');
Route::get('adminpublisherdelete/{id}','PublisherController@destroy');





Route::get('admingenrelist','GenreController@index');
Route::get('admingenrecreate','GenreController@create');
Route::post('admingenreinsert','GenreController@store');
Route::get('admingenreedit/{id}','GenreController@edit');
Route::post('admingenreupdate/{id}','GenreController@update');
Route::get('admingenredelete/{id}','GenreController@destroy');




